
app.controller('projectController', function ($scope, $cookies, projectService,$window) {
	gtag('event', 'page_view', {
		page_title: 'Project',
		page_location: '/project',
		page_path: '/project',
		send_to: 'G-B7VQKBGTE9'
	})
	$scope.displayText = $scope.chLangJson;
	$scope.projectList=[];

	$scope.lang = 1;
	//頁面中文內容
	$scope.chLangJson = {
		home: "首頁",
		title: "巨普專案",
		readMore: "了解更多",
		project: "專案",
		recent: "近期專案",
		moreService2: "沒有看到您要的服務？",
		moreServiceContent: "沒問題！我們提供客製化的服務，請告訴我們您的需求，我們將有專業團隊為您服務",
		contactUs: "聯絡我們"
	}
	//頁面英文內容
	$scope.enLangJson = {
		home: "Home",
		title: "Projects",
		readMore: "Read More",
		project: "Projects",
		recent: "RECENT PROJECTS",
		recentLink2: "Click to know more",
		moreService2: "Didn't see what you're looking for?",
		moreServiceContent: "Please tell us what you need, we will have our specialists help you!",
		contactUs: "CONTACT US"
	}

	//**取得專案內容**//
	$scope.getProjects = function () {
		projectService.getProjects().then(function (data) {
			if (data.code && data.code != 200) {
				chiperUtils.showErrorNotification("top", "center", data.msg);
			} else {
				$scope.projectList = [];
				for(let i in data){
				//	console.log(data[i])
				//	if(data[i].id!=10)
						$scope.projectList.push(data[i]);

				}
			}
		});
	};

	//**跳轉頁面到Project Detail**//
	$scope.goProjectDetail = function(id){
		$window.location.href = '#!/project-detail/'+id;
	}

	//**取得使用者選擇語言**//
	$scope.getLang = function () {
		let lang = $cookies.get('lang');
		if (lang == 1) {
			$scope.lang = 1;
			$scope.displayText = $scope.chLangJson;
		} else if (lang == 2) {
			$scope.lang = 2
			$scope.displayText = $scope.enLangJson;
		}
	};

	//頁面讀取後執行
	$scope.init = function () {
		$scope.getLang()//取得使用者選擇語言
		contentWayPoint();//設定頁面animation
		resetPosToTop();//回到頁面頂端
		$scope.getProjects();//取得專案內容
	}
	$scope.init();
});

