
app.controller('footerController', function ($scope, $cookies) {

	$scope.lang = 1;
	$scope.chLangJson={
		about:"關於巨普科技",
		aboutContent:'巨普科技是目前台灣CCTV業界最早投入CCD CAMERA開發生產的專業製造工廠，公司成立於1989年.成立第一年與"SONY"合作，成功研發台灣首支2/3”攝影機至今，一直對台灣監視業界之影像及控制產品的發展方向居於領導地位，近年來監視系統領域數位化及遠端監控已成為市場主流，所以管理的機制及設備的應用是產品開發首要考慮，本著一個年輕持續成長，公司研發部門已著手開發3DPTZ CAMERA，在未來安全E化的需求下，將會透過無死角的影像追蹤創造另一波影像監控與應用的革命。',
		contact:"聯絡方式",
		addressTitle:"Address",
		address:"22069新北市板橋區三民路二段33號4樓",
		phoneTitle:"電話",
		phone:"02-2964-6633",
		email:"support@chiper.com.tw"
	}
	$scope.enLangJson={
		about:"ABOUT CHIPER",
		aboutContent:'Founded in 1989, Chiper Technology has entered its third decade of operation. We were one of the pioneers in Taiwan that started R&D & manufacturing cameras in the field of CCTV system manufacturing. We were the first ones to be successful with R&D & production to supply 2/3" CCD cameras in Taiwan under cooperation with "SONY" corporation of Japan. Chiper has been a leader of Taiwan’s development on Video imaging & Control system equipment.',
		contact:"CONTACT",
		addressTitle:"Address",
		address:"4F., No. 33, Sec. 2, Sanmin Rd., Banqiao Dist., New Taipei City 220, Taiwan (R.O.C.)",
		phoneTitle:"Phone",
		phone:"+886-2-2964-6633",
		email:"support@chiper.com.tw"
	}

	$scope.scrollTop = function(){
		
		$("html, body").animate({ scrollTop: 0 }, "slow");

	}
	$scope.displayText = $scope.chLangJson;
	$scope.getLang = function () {
		let lang = $cookies.get('lang');

		if (lang == 1) {
			$scope.lang =1;
			$scope.displayText = $scope.chLangJson;
		} else if (lang == 2) {
			$scope.lang=2
			$scope.displayText = $scope.enLangJson;
		}
	};

	$scope.init = function(){
		$scope.getLang() 

	}
	$scope.init();
});

