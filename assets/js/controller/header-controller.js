
app.controller('headerController', function ($scope, $cookies, $location, $document) {

	$scope.changeLang = function (lang) {
		$cookies.put('lang', lang);
		location.reload();
	}

	$scope.lang = 1;
	$scope.chLangJson = {
		home: "首頁",
		about: "關於我們",
		project: "專案",
		product: "產品",
		carplate: "車牌辨識",
		carplateRoad: "一般道路/高速公路",
		carplateCommunity: "社區/學校/工廠",
		contact: "聯絡我們",
		lang: "中文/English"
	}
	$scope.enLangJson = {
		home: "Home",
		about: "About",
		project: "Project",
		product: "Product",
		carplate: "Carplate Recognition",
		carplateRoad: "Road/Highway",
		carplateCommunity: "Community/School/Factory",
		contact: "Contact Us",
		lang: "中文/English"

	}
	// 調整header color
	$('body').on('scroll', checkScroll);
	function checkScroll() {
		const currentPage = $location.path();
		const pageWidth = window.innerWidth;
		const scrolled = $('body').scrollTop() >= 5;
		if (pageWidth < 1198) {
			$('.nav-link').css("color", "white");
		}
		if (currentPage.includes("detail")) {
			handleDetailPage(scrolled, pageWidth);
		} else {
			handleOtherPages(scrolled);
		}
	}

	function handleDetailPage(scrolled, pageWidth) {
		if (pageWidth < 1198) {
			setNavStyle(scrolled, "rgba(0,0,0,0.7)", "transparent", "white");
		} else {
			setNavStyle(scrolled, "rgba(0,0,0,0.7)", "transparent", "white", "black");
		}
	}

	function handleOtherPages(scrolled) {
		setNavStyle(scrolled, "rgba(0,0,0,0.7)", "transparent", "white");
	}

	function setNavStyle(scrolled, scrolledColor, defaultColor, linkColor, defaultLinkColor = linkColor) {
		$('#nav').css("background-color", scrolled ? scrolledColor : defaultColor);
		$('.nav-link').css("color", scrolled ? linkColor : defaultLinkColor);
	}

	$scope.$on('$routeChangeStart', function () {
		adjustHeaderColor();
	});
	window.addEventListener("resize", adjustHeaderColor);

	function adjustHeaderColor() {
		const currentPage = $location.path();
		const pageWidth = window.innerWidth;
		const linkColor = currentPage.includes("detail") && pageWidth >= 1198 ? "black" : "white";
		$('.nav-link').css("color", linkColor);
	}

	// 調整header color end
	$scope.displayText = $scope.chLangJson;
	$scope.getLang = function () {
		let lang = $cookies.get('lang');
		if(!lang){
			lang=1;
		}
		if (lang == 1) {
			$scope.lang = 1;
			$scope.displayText = $scope.chLangJson;
		} else if (lang == 2) {
			$scope.lang = 2
			$scope.displayText = $scope.enLangJson;
		} 
		// else {
		// 	$('#lanModal').modal("show");
		// }


	};

	$scope.collapseNav = function () {
		$('#navbar').collapse('hide')

	}
	$scope.init = function () {
		$scope.getLang()
		contentWayPoint();
	}
	$scope.init();
});

