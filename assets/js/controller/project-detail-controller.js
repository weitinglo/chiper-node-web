
app.controller('projectDetailController', function ($scope, projectService, $routeParams, $cookies) {
	gtag('event', 'page_view', {
		page_title: 'project Detail',
		page_location: '/project_detail',
		page_path: '/project_detail',
		send_to: 'G-B7VQKBGTE9'
	})

	$scope.displayText = $scope.chLangJson;
	$scope.projectId = "";
	$scope.projectData = [];
	$scope.projectDesc = [];
	$scope.lang = 1;
	//頁面中文內容
	$scope.chLangJson = {
		type: "類型",
		download: "下載型錄",
		moreService: "沒有看到您要的服務？",
		moreServiceContent: "沒問題！我們提供客製化的服務，請告訴我們您的需求，我們將有專業團隊為您服務",
		contactUs: "聯絡我們",
		back: "回上一頁"
	}
	//頁面英文內容
	$scope.enLangJson = {
		type: "Type",
		download: "Download Catalog",
		moreService: "Didn't see what you're looking for?",
		moreServiceContent: "Please tell us what you need, we will have our specialists help you!",
		contactUs: "CONTACT US",
		back: "BACK"
	}

	//取得使用者選擇語言
	$scope.getLang = function () {
		let lang = $cookies.get('lang');
		if (lang == 1) {
			$scope.lang = 1;
			$scope.displayText = $scope.chLangJson;
		} else if (lang == 2) {
			$scope.lang = 2
			$scope.displayText = $scope.enLangJson;
		}
	};

	//嵌入video
	function embedIframe(videoSrc){
		var iframe = document.createElement('iframe');
		iframe.width = '560';
		iframe.height = '315';
		iframe.src = videoSrc;
		iframe.title = 'YouTube video player';
		iframe.frameBorder = '0';
		iframe.allow = 'accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share';
		iframe.allowFullscreen = true;
		iframe.referrerPolicy = 'strict-origin-when-cross-origin';
		// Insert the iframe into the container div
		var container = document.getElementById('videoContainer');
		container.appendChild(iframe);
	}

	//**取得Project 細節**//
	$scope.getProjectDetail = function (projectId) {
		projectService.getProjectDetail(projectId).then(function (data) {
			if (data.code && data.code != 200) {
				chiperUtils.showErrorNotification('top', 'center', data.msg)
			} else {
				let projectDetailData = data[0];
				if (projectDetailData.others) {
					projectDetailData.othersArr = JSON.parse(projectDetailData.others);
				}
				projectDetailData.presentType='image';
				//來源為影像
				if(projectDetailData.imageDetailSrc && projectDetailData.imageDetailSrc.includes("http")){
					projectDetailData.presentType='video';
					embedIframe(projectDetailData.imageDetailSrc);
				}
				$scope.projectData = projectDetailData;
			}
		});
	};

	//頁面讀取後執行
	$scope.init = function () {
		$scope.getLang();//取得使用者選擇語言
		$scope.projectId = $routeParams.projectId; //設定本頁project id
		var projectId = $routeParams.projectId; //設定本頁project id
		$scope.getProjectDetail(projectId);//取得Project 細節
		contentWayPoint();//設定頁面animation
		resetPosToTop();//回到頁面頂端
	}
	$scope.init();

});

