app.controller(
  "productController",
  function ($scope, productService, $cookies,$window) {
    gtag("event", "page_view", {
      page_title: "Product",
      page_location: "/product",
      page_path: "/product",
      send_to: "G-B7VQKBGTE9",
    });
    $scope.displayText = $scope.chLangJson;
    $scope.productData = [];
    $scope.productType = [];
    $scope.lang = 1;
    //頁面中文內容
    $scope.chLangJson = {
      home: "首頁",
      title: "巨普產品",
      product: "產品",
      moreService: "沒有看到您要的服務？",
      moreServiceContent:
        "沒問題！我們提供客製化的服務，請告訴我們您的需求，我們將有專業團隊為您服務",
      contactUs: "聯絡我們",
    };
    //頁面英文內容
    $scope.enLangJson = {
      home: "Home",
      title: "CHIPER Product",
      product: "Product",
      moreService: "Didn't see what you're looking for?",
      moreServiceContent:
        "Please tell us what you need, we will have our specialists help you!",
      contactUs: "CONTACT US",
    };

    //取得使用者選擇語言
    $scope.getLang = function () {
      let lang = $cookies.get("lang");
      if (lang == 1) {
        $scope.lang = 1;
        $scope.displayText = $scope.chLangJson;
      } else if (lang == 2) {
        $scope.lang = 2;
        $scope.displayText = $scope.enLangJson;
      }
      $scope.getProductType(lang);
      $scope.getProduct(lang);
    };

    //**取得Product  細節**//
    $scope.getProduct = function (lang) {
      productService.getProduct(lang).then(function (data) {
        if (data.code && data.code != 200) {
          chiperUtils.showErrorNotification("top", "center", data.msg);
        } else {
          // 1.這裡要先跑一層for loop
          for (let i = 0; i < data.length; i++) 
            if (data[i].img_arr !== null) // has data
              data[i].img_arr = data[i].img_arr.split(",");
        }
        $scope.productData = data;
      });
    };

    //**取得Product Type 細節**//
    $scope.getProductType = function (lang) {
      productService.getProductType(lang).then(function (data) {
        if (data.code && data.code != 200) {
          chiperUtils.showErrorNotification("top", "center", data.msg);
        } else {
          // for(let i in data){
          //   $scope.productType.push(data[i])
          // }
          if(lang==1){
            $scope.productType = [
              // {
              //   type: "智慧型標籤預警系統",
              // },
              {
                type: "車牌辨識攝影機",
              },
              {
                type: "防爆設備",
              },
              {
                type: "旋轉台",
              },
              {
                type: "紅外線投射燈",
              },
              {
                type: "透霧HDR設備",
              },
              {
                type: "周邊設備",
              },
              {
                type: "生物特徵辨識",
              },
            ];
  
          }else{
            $scope.productType = [
              // {
              //   type: "智慧型標籤預警系統",
              // },
              {
                type: "CAR PLATE RECOGNITION CAMERA",
              },
              {
                type: "Explosion-Proof",
              },
              {
                type: "Pan Tilt",
              },
              {
                type: "IR Illuminator",
              },
              {
                type: "Defog and HDR",
              },
              {
                type: "Accessory",
              },
              {
                type: "Biometrics",
              },
            ];
          }
        }
      });
    };

    //頁面跳轉到產品細節
    $scope.goProductDetail = function(id){
      $window.location.href = '#!/product-detail/'+id;
    }

    //頁面讀取後執行
    $scope.init = function () {
      $scope.getLang();//取得使用者選擇語言
      contentWayPoint();//設定頁面animation
      resetPosToTop();//回到頁面頂端
    };
    $scope.init();
  }
);
