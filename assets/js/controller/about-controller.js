
app.controller('aboutController', function ($scope,$cookies) {
   
	gtag('event', 'page_view', {
		page_title: 'About',
		page_location: '/about',
		page_path: '/about',
		send_to: 'G-B7VQKBGTE9'
	});

	$scope.displayText = $scope.chLangJson;

	$scope.lang = 1;
	//頁面中文內容
	$scope.chLangJson={
		home:"首頁",
		aboutTitle:"關於我們",
		about:"關於",
		aboutChiper:"關於巨普",
		aboutChiperTitle1:"於",
		aboutChiperTitle2:"1989四月",
		aboutChiperTitle3:"我們成立了",
		aboutChiperContent:'巨普科技是目前台灣CCTV業界最早投入CCD CAMERA開發生產的專業製造工廠，公司成立於1989年。成立第一年與 SONY 合作，成功研發台灣首支2/3”攝影機。至今，一直對台灣監視業界之影像及控制產品的發展方向居於領導地位，近年來監視系統領域數位化及遠端監控已成為市場主流，所以管理的機制及設備的應用是產品開發首要考慮，本著一個年輕持續成長，公司研發部門已著手開發3D PTZ CAMERA，在未來安全E化的需求下，將會透過無死角的影像追蹤創造另一波影像監控與應用的革命。',
		aboutChiperContent2:'巨普科技是目前台灣CCTV業界最早投入CCD CAMERA開發生產的專業製造工廠，公司成立於1989年。成立第一年與 SONY 合作，成功研發台灣首支2/3”攝影機。至今，一直對台灣監視業界之影像及控制產品的發展方向居於領導地位，近年來監視系統領域數位化及遠端監控已成為市場主流，所以管理的機制及設備的應用是產品開發首要考慮，本著一個年輕持續成長，公司研發部門已著手開發3D PTZ CAMERA，在未來安全E化的需求下，將會透過無死角的影像追蹤創造另一波影像監控與應用的革命。',
		aboutChiperContent3:"近幾年來，世界進入了人工智慧的時代，巨普致力於研發更精準且多元化的智慧判識系統，目前已運用在許多領域上如道路車流控管、鐵路、輕軌安全和工廠自動化...等。",
		aboutChiperContent4:'三十多年以來，我們不斷以打造智慧且安全的城市為我們的願景，以此為目標，不斷的挑戰自己和運用最新的科技來滿足客戶最高的期望。',
		aboutChiperContent5:"",
		aboutChiperContent6:"",
		securityPolicyTitle:"資安政策",
		securityPolicyContent:"為維護巨普科技股份有限公司（以下簡稱本公司）所屬資訊資產之機密性、完整性及可用性，並符合相關法規之要求，並保障使用者資料隱私，使其免於遭受內、外部的蓄意或意外之威脅。本公司整合各層級部門資通安全目標，建立本公司整體資通安全政策，並執行符合相關法規之要求，確保法規遵循性。",
		chiperCertification:"巨普認證"
	}
	//頁面英文內容
	$scope.enLangJson={
		home:"Home",
		aboutTitle:"About Us",
		about:"About",
		aboutChiper:"About Chiper",
		aboutChiperTitle1:"FOUNDED IN",
		aboutChiperTitle2:"1989 APRIL",
		aboutChiperTitle3:"",
		aboutChiperContent:'Founded in 1989, Chiper Technology has entered its third decade of operation. We were one of the pioneers in Taiwan that started R&D & manufacturing cameras in the field of CCTV system manufacturing. We were the first ones to be successful with R&D & production to supply 2/3" CCD cameras in Taiwan under cooperation with "SONY" corporation of Japan. Chiper has been a leader of Taiwan’s development on Video imaging & Control system equipment. Recently, CCTV has entered an era where it has to involve digitalization & remote monitoring through the internet. This has become the major trend in the market demand. This situation also has made "CCTV administration method" & "CCTV Equipment operation habit" as ChipER’s fundamental consideration during R&D of any new product. Under demand for E-security & Video security, we will continue to revolutionize new era security application for each of every invincible corner.',
		aboutChiperContent2:'Chiper has more than 30 years of experience in the security industry and keeps deepening its knowledge and experience in meeting customer needs. We have  provided our solutions and products to many fields, including public security, transportation, military, industrial automation and many others.',
		aboutChiperContent3:"In recent years, we embrace AI technology and utilize machine learning and deep learning to enhance our security solutions, such as car plate recognition, traffic volume control and train & rail transit safety control. With this technology, it helps us on building an automatic security system that's not only able to detect dangerous events immediately, but also gather and record useful data for further analytical purposes.",
		aboutChiperContent4:'In addition, we dedicate on building a smart city where data can be used to operate across a city. Smart railway system is our first step on this path. The system is already in operation in many places where users can easily know where the trains are and the condition of railway paths online.',
		aboutChiperContent5:"Our company's vision is to improve people's lives through innovative security solutions and create a smarter and safer world. ",
		aboutChiperContent6:"We never stop challenging ourselves to ensure our security solutions are packed with the latest and meaningful technologies that are able to help our customers achieve maximum benefit.",
		securityPolicyTitle:"Information Security Policy",
		securityPolicyContent:'To safeguard the confidentiality, integrity, and availability of the information assets of Chiper Technology Co., Ltd. (hereinafter referred to as "the Company"), comply with relevant regulations, and protect user data privacy from intentional or accidental internal and external threats, the Company integrates information security objectives at all departmental levels. The Company establishes an overall information security policy and implements measures to meet relevant regulatory requirements, ensuring compliance.',
		
		chiperCertification:"CERTIFICATE"
	}
	//**取得使用者選擇語言**//
	$scope.getLang = function () {
		let lang = $cookies.get('lang');

		if (lang == 1) {
			$scope.lang =1;
			$scope.displayText = $scope.chLangJson;
		} else if (lang == 2) {
			$scope.lang=2
			$scope.displayText = $scope.enLangJson;
		} 

	};
	
	//頁面讀取後執行
	$scope.init = function(){
		$scope.getLang() //取得使用者選擇語言
		contentWayPoint();//設定頁面animation
		resetPosToTop();//回到頁面頂端
	}
	$scope.init();
});

