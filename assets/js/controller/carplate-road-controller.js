
app.controller('carplateRoadController', function ($scope, $cookies) {
	gtag('event', 'page_view', {
		page_title: 'Carplate',
		page_location: '/carplate',
		page_path: '/carplate',
		send_to: 'G-B7VQKBGTE9'
	})
	$scope.displayText = $scope.chLangJson;
	$scope.lang = 1;
	//頁面中文內容
	$scope.chLangJson={
		title:"AI 雲端車牌辨識",
		orderNow:"馬上訂購",
		customize:"客製化調整",
		customizeContent:"根據您所在位置的光照、角度、天氣為基礎，時時調整和更新以達到最完整的辨識效果",
		dataAnalysis:"數據分析",
		dataAnalysisContent:"一目了然的數據分析，車流量、停車率、收費統計等，平台上幫您一次統整",
		platform:"完整平台",
		platformContent:'統整所有地區的資訊，做最有效的車流、停車管理，不管您在任何地方，隨時隨地查看所有資訊',
		plugAndUse:"隨接隨用",
		plugAndUseContent:"簡單方便的安裝方式，無須更改現有的裝置，即可以使用最新的AI雲端技術",
		demo:"實際畫面",
		demoSearch:"搜尋車輛行為模式",
		demoRoad:"道路辨識",
		applicationField:"適合場所",
		service1:"學校",
		service2:"社區",
		service3:"工廠",
		service4:"道路",
		service5:"公司",
		service6:"百貨公司",
		service7:"停車場",
		service8:"遊樂園",
		moreService:"沒有看到您要的服務？",
		moreServiceContent:"沒問題！我們提供客製化的服務，請告訴我們您的需求，我們將有專業團隊為您服務",
		contactUs:"聯絡我們"
	}
	//頁面英文內容
	$scope.enLangJson={
		title:"AI Carplate Recognition Platform",
		orderNow:"Order Now",
		customize:"Customized",
		customizeContent:"To give you the best accuracy, we update our system accordingly based on your camera's current angle, lighting, and weather conditions.",
		dataAnalysis:"Data Analysis",
		dataAnalysisContent:"Gather all the information and give you the most straightforward and clear results, like traffic flows, parking rate, and payment amount.",
		platform:"One Platform",
		platformContent:'Manage all your locations with the latest infromation anywere on any devices.',
		plugAndUse:"Easy Installation",
		plugAndUseContent:"Work with exisiting infrastructure. Just simply plug and use!",
		demo:"Demo",
		demoSearch:"Search Vehicle Behavior",
		demoRoad:"Detection on Roads",
		applicationField:"Suitable Places",
		service1:"School",
		service2:"Apartment Complex",
		service3:"Factory",
		service4:"Roads",
		service5:"Company Parking Lot",
		service6:"Mall",
		service7:"Parking Lot",
		service8:"Amusement Park",
		moreService:"Didn't see what you're looking for?",
		moreServiceContent:"Please tell us what you need, we will have our specialists help you!",
		contactUs:"CONTACT US"
		
	}

	//取得使用者選擇語言
	$scope.getLang = function () {
		let lang = $cookies.get('lang');
		if (lang == 1) {
			$scope.lang =1;
			$scope.displayText = $scope.chLangJson;
		} else if (lang == 2) {
			$scope.lang=2
			$scope.displayText = $scope.enLangJson;
		} 
	};

	//頁面讀取後執行
	$scope.init = function(){
		$scope.getLang();//取得使用者選擇語言
		contentWayPoint();//設定頁面animation
		resetPosToTop();//回到頁面頂端
	}
	$scope.init();
});

