
app.controller('aiTagController', function ($scope, productService,$routeParams, $cookies) {
	gtag('event', 'page_view', {
		page_title: 'Product Detail',
		page_location: '/product_detail',
		page_path: '/product_detail',
		send_to: 'G-B7VQKBGTE9'
	})
	
	$scope.productId = "";
	$scope.productData = [];
	$scope.productDesc = [];
	$scope.lang = 1;
	$scope.chLangJson={
		type:"類型",
		download:"下載型錄",
		moreService:"沒有看到您要的服務？",
		moreServiceContent:"沒問題！我們提供客製化的服務，請告訴我們您的需求，我們將有專業團隊為您服務",
		contactUs:"聯絡我們"
	}
	$scope.enLangJson={
		type:"Type",
		download:"Download Catalog",
		moreService:"Didn't see what you're looking for?",
		moreServiceContent:"Please tell us what you need, we will have our specialists help you!",
		contactUs:"CONTACT US"
	}

	$scope.displayText = $scope.chLangJson;
	$scope.getLang = function () {
		let lang = $cookies.get('lang');

		if (lang == 1) {
			$scope.lang =1;
			$scope.displayText = $scope.chLangJson;
		} else if (lang == 2) {
			$scope.lang=2
			$scope.displayText = $scope.enLangJson;
		}	

	};
	$scope.getProductDetail = function (productId) {
		productService.getProductDetail(productId,$scope.lang).then(function (data) {
			if (data.code && data.code != 200) {
				chiperUtils.showErrorNotification('top', 'center', data.msg)
			} else{
				$scope.productData = data[0];
				$("#productImg").attr("src",$scope.productData.img);

				if($scope.productData.img_arr!==null){
					$scope.productData.img_arr = $scope.productData.img_arr.split(",");				
					for(let i in $scope.productData.img_arr){
						$scope.productData.img_arr[i] = $scope.productData.img_arr[i].replace("\n","").trim();
					}
							//解決reel 讀取問題
					$("#productImg").one("load",
						function() {
							$('#productImg').reel({
								frames: 24, 
								speed:0.1,
								img:$scope.productData.img,
								images:$scope.productData.img_arr
							});
						}
					);
				}else{
					$("#productImg").css("max-width","600px");
					$("#productImg").attr("width","100%");

					$("#productImg").attr("height","auto");

				}

		
				
			}
		});
	};

	$scope.init = function(){
		$scope.getLang();
		$scope.productId = $routeParams.productId;
		var productId = $routeParams.productId;
		$scope.getProductDetail(productId);
		contentWayPoint();
		
	}
	$scope.init();
	
});

