
app.controller('homeController', function ($scope, $cookies,$window) {
	gtag('event', 'page_view', {
		page_title: 'Home',
		page_location: '/home',
		page_path: '/home',
		send_to: 'G-B7VQKBGTE9'
	});
	$scope.displayText = $scope.chLangJson;
	$scope.lang = 1;
	//頁面中文內容
	$scope.chLangJson = {
		slideTitle1: "",
		slideSubtitle1:"2024年防災科技應用技術優質獎",
		slideTitle2: "巨普科技-安控的新革命",
		slideSubtitle2:"安控的新革命",
		title: "巨普科技-安控的新革命",
		titleVid: "看影片",
		lastestTitle: "最新產品: AI 人形辨識系統",
		about: "我們是誰？",
		aboutTitle1: "安控的",
		aboutTitle2: "最前線",
		aboutTitle3: "重新定義",
		aboutContent: '巨普科技是目前台灣CCTV業界最早投入CCD CAMERA開發生產的專業製造工廠，公司成立於1989年。成立第一年與 SONY 合作，成功研發台灣首支2/3”攝影機。至今，一直對台灣監視業界之影像及控制產品的發展方向居於領導地位，近年來監視系統領域數位化及遠端監控已成為市場主流，所以管理的機制及設備的應用是產品開發首要考慮，本著一個年輕持續成長，公司研發部門已著手開發3D PTZ CAMERA，在未來安全E化的需求下，將會透過無死角的影像追蹤創造另一波影像監控與應用的革命。',
		knowMore: "了解更多",
		latestProject: "最新方案",
		carplate: "車牌辨識",
		carplateContent: "提供您人性且客製化的車辨服務",
		lidar: "光達方案",
		lidarContent: "利用３Ｄ呈像技術，實現實時準確的物體偵測",
		cctv: "監控設備",
		cctvContenr: "惡劣環境，防爆，交給我們",
		rail: "鐵路防闖",
		railContent: "道路的安全，我們控管",
		moreProject: "更多方案",
		aiCarplate: "AI 雲端車牌辨識系統",
		aiCarplate1: "環境適應",
		aiCarplateContent1: "根據您所在位置的光照、角度、天氣做最客製化的更新，不再擔心辨識率的問題",
		aiCarplate2: "數據分析",
		aiCarplateContent2: "一目了然的數據分析，車流量、停車率、收費統計等，平台上幫您一次統整",
		aiCarplate3: "雲端整合",
		aiCarplateContent3: "統整所有地區的資訊，做最有效的車流、停車管理，不管您在任何地方，隨時隨地查看所有資訊",
		aiCarplateCommunity: "社區方案",
		aiCarplateRoad: "了解方案",
		hotProducts: "熱門產品",
		moreService: "沒有看到您要的服務？",
		moreServiceContent: "沒問題！我們提供客製化的服務，請告訴我們您的需求，我們將有專業團隊為您服務",
		contactUs: "聯絡我們"
	}
	//頁面英文內容
	$scope.enLangJson = {
		slideTitle1: "2024",
		slideSubtitle1:"Disaster Prevention Excellence Award",
		slideTitle2: "CHIPER TECH-REDEFINING SECURITY",
		slideSubtitle2:"",
		title: "CHIPER TECH-REDEFINING SECURITY",
		titleVid: "Video",
		lastestTitle: "New: AI People Detection System",
		about: "ABOUT US",
		aboutTitle1: "THE",
		aboutTitle2: "LEADER",
		aboutTitle3: "IN SECURITY INDUSTRY",
		aboutContent: 'Founded in 1989, Chiper Technology has entered its third decade of operation. We were one of the pioneers in Taiwan that started R&D & manufacturing cameras in the field of CCTV system manufacturing. We were the first ones to be successful with R&D & production to supply 2/3" CCD cameras in Taiwan under cooperation with "SONY" corporation of Japan. Chiper has been a leader of Taiwan’s development on Video imaging & Control system equipment. Recently, CCTV has entered an era where it has to involve digitalization & remote monitoring through the internet. This has become the major trend in the market demand. This situation also has made "CCTV administration method" & "CCTV Equipment operation habit" as ChipER’s fundamental consideration during R&D of any new product. Under demand for E-security & Video security, we will continue to revolutionize new era security application for each of every invincible corner.', knowMore: "MORE INFO",
		latestProject: "THE LATEST SOLUTION",
		carplate: "CAR PLATE RECOGNITION",
		carplateContent: "Not only fast and accurate, but also customize it to fit your most challenging needs",
		lidar: "Lidar Solution",
		lidarContent: "3D Modeling, give you the entire protection with no limit",
		cctv: "Surveillance Products",
		cctvContenr: "Empowering your safety even in a challenging environment",
		rail: "Railway Safety",
		railContent: "Many years of experience in railway safety",
		moreProject: "MORE SOLUTION",
		aiCarplate: "AI Carplate Recognition System",
		aiCarplate1: "Environmental Adaptation",
		aiCarplateContent1: "To give you the best accuracy, we update our system accordingly base on your camera's current angle, lighting, and weather conditions",
		aiCarplate2: "Data Analysis",
		aiCarplateContent2: "Gather all the information and give you the most straightforward and clear results, like traffic flows, parking rate, and payment amount. ",
		aiCarplate3: "One Platform",
		aiCarplateContent3: "Manage all your locations with the latest infromation anywere on any devices",
		aiCarplateCommunity: "COMMUNITY",
		aiCarplateRoad: "Check it out",
		hotProducts: "BEST SELLERS",
		moreService: "Didn't see what you're looking for?",
		moreServiceContent: "Please tell us what you need, we will have our specialists help you!",
		contactUs: "CONTACT US"

	}
	$scope.toUDAS = function(){
		$window.location.href = '#!/project-detail/10';
	}
	$scope.toProductPage = function(){
		$window.location.href = '#!/product';
	}
	$scope.toProductDetailPage = function(num) {
        $window.location.href = '#!/product-detail/' + num;
    };

    // Attach function to global window object so onClick can access it
    window.toProductDetailPage = function(num) {
        $scope.$apply(function() {
            $scope.toProductDetailPage(num);
        });
    };
	// $scope.toCarplateSection = function () {
	// 	$('html, body').animate({
	// 		scrollTop: $("#carplateSection").offset().top
	// 	}, 1000);
	// }

	//**取得使用者選擇語言**//
	$scope.getLang = function () {
		let lang = $cookies.get('lang');
		if (lang == 1) {
			$scope.lang = 1;
			$scope.displayText = $scope.chLangJson;
		} else if (lang == 2) {
			$scope.lang = 2
			$scope.displayText = $scope.enLangJson;
		}
	};
	$scope.currentIndex=0;
	$scope.slideLen=2;

      // Function to go to the next slide
      $scope.nextSlide = function() {
        $scope.currentIndex = ($scope.currentIndex + 1) % $scope.slideLen;
      };

      // Function to go to the previous slide
      $scope.prevSlide = function() {
        $scope.currentIndex = ($scope.currentIndex - 1 + $scope.slideLen) % $scope.slideLen;
      };


	//頁面讀取後執行
	$scope.init = function () {
		$scope.getLang();//取得使用者選擇語言
		homeProductSlide();//設定熱門產品slider
		contentWayPoint();//設定頁面animation
		resetPosToTop();//回到頁面頂端

	}
	$scope.init();


});

