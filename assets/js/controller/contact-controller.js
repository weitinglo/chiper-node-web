
app.controller('contactController', function ($scope, $cookies, contactService) {
	gtag('event', 'page_view', {
		page_title: 'Contact',
		page_location: '/contact',
		page_path: '/contact',
		send_to: 'G-B7VQKBGTE9'
	})
	$scope.displayText = $scope.chLangJson;
	$scope.name = "";
	$scope.email = "";
	$scope.phone = "";
	$scope.subject = "";
	$scope.message = "";
	$scope.lang = 1;

	//頁面中文內容
	$scope.chLangJson = {
		title: "聯絡我們",
		home: "首頁",
		contact: "聯絡",
		taiwanOffice: "台灣辦公室（總部）",
		taiwanOfficeAdd: "22069新北市板橋區三民路二段33號4樓",
		taiwanOfficeTel: "02-2964-6633",
		taiwanOfficeEmail: "chiper@chiper.com.tw",
		usOffice: "美國辦公室",
		usOfficeAdd: "6907 University Ave #299, Middleton, WI 53562",
		usOfficeTel: "608-274-2475",
		usOfficeEmail: "wei@chipertechs.com",
		contactInfo: "如果有任何問題，請不要客氣與我們聯絡，我們將會有專人為您服務！",
		contactName: "您的名字",
		contactPhone: "電話",
		contactSubject: "關於主題",
		contactMessage: "給我們的訊息",
		contactSend: "寄出",
		successMessage: "感謝您的留言，我們會有專人與您聯絡",
		errorMessage: "請完整填完資訊",
		errorEmailMessage: "請輸入正確的email格式",
		errorPhoneMessage: "電話號碼只能填數字"
	}
	//頁面英文內容
	$scope.enLangJson = {
		title: "CONTACT US",
		home: "Home",
		contact: "Contact Us",
		taiwanOffice: "Taiwan Office (Headquarter)",
		taiwanOfficeAdd: "4F., No. 33, Sec. 2, Sanmin Rd., Banqiao Dist., New Taipei City 220, Taiwan (R.O.C.)",
		taiwanOfficeTel: "+886-2-2964-6633",
		taiwanOfficeEmail: "chiper@chiper.com.tw",
		usOffice: "United States",
		usOfficeAdd: "6907 University Ave #299, Middleton, WI 53562",
		usOfficeTel: "608-274-2475",
		usOfficeEmail: "wei@chipertechs.com",
		contactInfo: "Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within a matter of hours to help you.",
		contactName: "Name",
		contactPhone: "Phone",
		contactSubject: "Subject",
		contactMessage: "Your Message",
		contactSend: "Send",
		successMessage: "Thanks for contacting us, we will get back to you soon!",
		errorMessage: "Please fill all the required information.",
		errorEmailMessage: "Please enter a valid email address",
		errorPhoneMessage: "Phone number can only contain digits"
	}
	//取得使用者選擇語言
	$scope.getLang = function () {
		let lang = $cookies.get('lang');
		if (lang == 1) {
			$scope.lang = 1;
			$scope.displayText = $scope.chLangJson;
		} else if (lang == 2) {
			$scope.lang = 2
			$scope.displayText = $scope.enLangJson;
		}
	};
	//寄出留言表單
	$scope.send = function () {
		if ($scope.name == "" || $scope.email == "" || $scope.subject == "") {
			chiperUtils.showErrorNotification('top', 'center', $scope.displayText.errorMessage)
			return
		}
		const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;  //// 驗證 email 格式
		if (!emailPattern.test($scope.email)) {
			chiperUtils.showErrorNotification('top', 'center', $scope.displayText.errorEmailMessage);
			return;
		}
		const phonePattern = /^[0-9]+$/;                    //// 驗證電話格式，只能打數字
		if (!phonePattern.test($scope.phone)) {
			chiperUtils.showErrorNotification('top', 'center', $scope.displayText.errorPhoneMessage);
			return;
		}
		contactService.send($scope.name, $scope.phone, $scope.email, $scope.subject, $scope.message).then(function (data) {
			if (data.code && data.code != 200) {
				chiperUtils.showErrorNotification('top', 'center', $scope.displayText.errorMessage)
			} else {
				chiperUtils.showSuccessNotification('top', 'center', $scope.displayText.successMessage)
			}
		});
	}
	//頁面讀取後執行
	$scope.init = function () {
		$scope.getLang();//取得使用者選擇語言
		contentWayPoint();//設定頁面animation
		resetPosToTop();//回到頁面頂端
	};
	$scope.init();
});
