var contentWayPoint = function () {
  var i = 0;
  $(".element-animate").waypoint(
    function (direction) {
      if (
        direction === "down" &&
        !$(this.element).hasClass("element-animated")
      ) {
        i++;
        $(this.element).addClass("item-animate");
        setTimeout(function () {
          $("body .element-animate.item-animate").each(function (k) {
            var el = $(this);
            setTimeout(function () {
              var effect = el.data("animate-effect");
              if (effect === "fadeIn") {
                el.addClass("fadeIn element-animated");
              } else if (effect === "fadeInLeft") {
                el.addClass("fadeInLeft element-animated");
              } else if (effect === "fadeInRight") {
                el.addClass("fadeInRight element-animated");
              } else {
                el.addClass("fadeInUp element-animated");
              }
              el.removeClass("item-animate");
            }, k * 100);
          });
        }, 100);
      }
    },
    { offset: "95%" }
  );
};
var resetPosToTop = function () {
  document.body.scrollTop = document.documentElement.scrollTop = 0;
};

var homeProductSlide = function () {
  $(".nonloop-block-11").owlCarousel({
    center: false,
    items: 1,
    loop: true,
    autoplay: true,
    stagePadding: 20,
    margin: 50,
    nav: true,
    smartSpeed: 1000,
    navText: [
      '<span class="ion-chevron-left">',
      '<span class="ion-chevron-right">',
    ],
    responsive: {
      600: {
        stagePadding: 20,
        items: 1,
      },
      800: {
        stagePadding: 20,
        items: 2,
      },
      1000: {
        // stagePadding: 200,
        items: 2,
      },
    },
  });
};

function engLan() {
  $.cookie("lang", "2");
  location.reload();
}
function chLan() {
  $.cookie("lang", "1");
  location.reload();
}
$(document).ready(function () {
  let lan = $.cookie("lang");
  if (!lan) {
    $.cookie("lang", "1");
    // $("#lanModal").modal("show");
  }
});

var chiperUtils = {
  misc: {
    navbar_menu_visible: 0,
  },

  showNotification: function (from, align, msg) {
    color = "info";
    $.notify(
      {
        icon: "fa fa-bell",
        message: msg,
      },
      {
        type: color,
        timer: 1000,
        placement: {
          from: from,
          align: align,
        },
        template:
          '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
          '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
          '<span data-notify="icon"></span> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
          '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          "</div>" +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
          "</div>",
      }
    );
  },
  showErrorNotification: function (from, align, msg) {
    color = "danger";
    $.notify(
      {
        icon: "fa fa-exclamation-triangle",
        message: msg,
      },
      {
        type: color,
        timer: 1000,
        placement: {
          from: from,
          align: align,
        },
        template:
          '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
          '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
          '<span data-notify="icon"></span> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
          '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          "</div>" +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
          "</div>",
      }
    );
  },
  showSuccessNotification: function (from, align, msg) {
    color = "success";
    $.notify(
      {
        icon: "fa fa-check-circle",
        message: msg,
      },
      {
        type: color,
        timer: 1000,
        placement: {
          from: from,
          align: align,
        },
        template:
          '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
          '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
          '<span data-notify="icon"></span> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
          '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          "</div>" +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
          "</div>",
      }
    );
  },
};
