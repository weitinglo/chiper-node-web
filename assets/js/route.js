app.config(['$routeProvider',
function ($routeProvider) {
  $routeProvider
    .when('/home', {
      templateUrl: '/home.htm',
      controller: 'homeController'
    })
    .when('/about', {
      templateUrl: '/about.htm',
      controller: 'aboutController'
    })
    .when('/project', {
      templateUrl: '/project.htm',
      controller: 'projectController'
    })
    .when('/project-detail/:projectId', {
      templateUrl: '/project-detail.htm',
      controller: 'projectDetailController'
    })
    .when('/product', {
      templateUrl: '/product.htm',
      controller: 'productController'
    })
    .when('/product-detail/:productId', {
      templateUrl: '/product-detail.htm',
      controller: 'productDetailController'
    })
    .when('/service-detail/:serviceId', {
      templateUrl: '/service-detail.htm',
      controller: 'serviceDetailController'
    })
    .when('/contact', {
      templateUrl: '/contact.htm',
      controller: 'contactController'
    })
    .when('/contact_view', {
      templateUrl: '/contact_view.htm',
      controller: 'contactViewController'
    })
    .when('/carplate-road', {
      templateUrl: '/carplate-road.htm',
      controller: 'carplateRoadController'
    })
    .when('/carplate-community', {
      templateUrl: '/carplate-community.htm',
      controller: 'carplateCommunityController'
    })
    .when('/message', {
      templateUrl: '/message.htm',
      controller: 'messageController'
    })
    .when('/ai-tag/:productId', {
      templateUrl: '/ai-tag.htm',
      controller: 'aiTagController'
    })
    .otherwise({
      redirectTo: '/home'
    });
}]);