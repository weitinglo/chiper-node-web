
app.factory("contactService", function ($http) {
    return {
        send: function (name, phone, email, sub, message) {
            return $http.post('contact_message', {
                'name': name,
                'phone': phone,
                'email': email,
                'sub': sub,
                'message': message
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getContact: function (name,pwd) {
            return $http.post('get_contact', {
                'name': name,
                'pwd': pwd
            })
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
    }
})
