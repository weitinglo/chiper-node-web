
app.factory("projectService", function ($http) {
    return {
        getProjects: function () {
            return $http.get('project')
                .then(function (response) {
                    console.log(response.data);
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getProjectDetail: function (projectId) {
            return $http.get('project_detail?projectId='+projectId)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        }
    }
})
