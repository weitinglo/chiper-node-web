
app.factory("productService", function ($http) {
    return {
        getProduct: function (lang) {
            return $http.get('product?lang='+lang)
                .then(function (response) {
                    // console.log(response.data);
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getProductType: function (lang) {
            return $http.get('product_type?lang='+lang)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        },
        getProductDetail: function (productId,lang) {
            return $http.get('product_detail?productId='+productId+"&lang="+lang)
                .then(function (response) {
                    return response.data;
                }, function (response) {
                    return response;
                });
        }
    }
})
