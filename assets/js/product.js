function getUrlParameter(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
  var results = regex.exec(location.search);
  return results === null
    ? ""
    : decodeURIComponent(results[1].replace(/\+/g, " "));
}
$(function () {
  var id = getUrlParameter("id");
  switch (id) {
    case "1":
      $("#name").text("夜視型網路車道全景攝影機");
      $("#code").text("CPT-SHR150");
      $("#type").text("車牌辨識攝影機");
      $("#description").text(
        "-Super H-IR 新一代高亮度紅外線元件\n-搭配攝影機一體化設計\n-HD高畫質,解析度可達1080P\n-夜間有限路燈光線下,可清晰拍攝多車道路況及車牌\n-連續使用下,工作壽命可達10,000小時以上,符合防水防塵等級IP-68"
      );
      $("#img_path").attr("src", "images/product/CPT-SHR600HD.jpg");
      $("#doc_path").attr("href", "specs/CPT-SHR150-tw.pdf");
      break;
    case "2":
      $("#name").text("高速紅外線網路車辨攝影機");
      $("#code").text("CPT-SHR750");
      $("#type").text("車牌辨識攝影機");
      $("#description").text(
        "-Super H-IR 新一代高亮度紅外線元件,搭配攝影機一體化設計 \n-HD高畫質,解析度可達1280x1024@30fps \n-夜間有限路燈光線下,可清晰拍攝多車道路況及車牌 \n-連續使用下,工作壽命可達10,000小時以上 \n-符合防水防塵等級IP-6"
      );
      $("#img_path").attr("src", "images/product/CPT-SHR600HD.jpg");
      $("#doc_path").css("display", "none");
      break;
    case "3":
      $("#name").text("高速紅外線網路車辨攝影機");
      $("#code").text("CPT-SHR600HD3");
      $("#type").text("車牌辨識攝影機");
      $("#description").text(
        "-Super H-IR 新一代高亮度紅外線元件,搭配攝影機一體化設計\n-Full HD高畫質,解析度可達1080P@60fps\n-日/夜間道路上,可清晰辨識150Km以上車速之前/後車牌.達98%以上\n-連續使用下,工作壽命可達20,000小時以上\n-符合防水防塵等級IP-6"
      );
      $("#img_path").attr("src", "images/product/CPT-SHR600HD.jpg");
      $("#doc_path").attr("href", "specs/CPT-SHR600HD3-en.pdf");
      break;
    case "4":
      $("#name").text("除霧鏡頭");
      $("#code").text("CPT-LNS-NIR10330D-2MAF");
      $("#type").text("透霧HDR設備");
      $("#description").text(
        "-2百萬畫素高動能鏡頭\n-智能除霧\n-全自動聚焦\n-420nm ~ 1105nm 近紅外線"
      );
      $("#img_path").attr("src", "images/product/defog-len.jpg");
      $("#doc_path").attr(
        "href",
        "specs/Catalog-NIR 33X Lens_CPT-LNS-NIR10330D-2MAF_en_spec.pdf"
      );
      break;
    case "5":
      $("#name").text("除霧鏡頭");
      $("#code").text("CPT-LNS-NIR10330D-3MA");
      $("#type").text("透霧HDR設備");
      $("#description").text(
        "-3百萬畫素高動能鏡頭\n-智能除霧\n-全自動聚焦\n-420nm ~ 1105nm 近紅外線"
      );
      $("#img_path").attr("src", "images/product/defog-len2.jpg");
      $("#doc_path").attr(
        "href",
        "specs/Catalog-NIR 60X Lens_CPT-LNS-NIR125750D-3MAF_en_spec.pdf"
      );
      break;
    case "6":
      $("#name").text("除霧攝影機");
      $("#code").text("CPT-IPC-8100B");
      $("#type").text("透霧HDR設備");
      $("#description").text(
        "-2百萬畫素逐行掃描CMOS\n-支援 H.265 & H.264 編碼\n-搭配智能除霧和HDR功能\n-420nm ~ 1105nm 近紅外線"
      );
      $("#img_path").attr("src", "images/product/defog-camera.jpg");
      $("#doc_path").attr(
        "href",
        "specs/Chiper_IPC_Defog_Srarvis_CPT-IPC-8100B-DF_en.pdf"
      );
      break;
    case "7":
      $("#name").text("耐環境氣侯 不鏽鋼紅外線防爆型防護罩");
      $("#code").text("CPT-EXH1840");
      $("#type").text("防爆設備");
      $("#description").text(
        "-防撞擊鋼化光學視窗玻璃，清晰明亮適合高倍數鏡頭監控使用\n-光學視窗, 熱電分離設計, 光學攝影不受光源影響\n-高功率紅外線 , 投射距離覆蓋 5-150公尺\n-符合 CNS14165,CNS3376-0, IEC60529 防塵防水 IP-68 標準\n-內裝除霧電熱器並使用自動溫度開關控制\n-適用安裝場所：煉油廠、加油站、瓦斯充氣站、汙水處理、火力發電、礦場、晶圓廠、生物科技、化學工廠、海邊、鹽害、海域等易燃、腐蝕、易爆、油庫及彈藥庫等特殊環境場所"
      );
      $("#img_path").attr("src", "images/product/CPT-EXH1840-IR.jpg");
      $("#doc_path").attr("href", "specs/CPT-EXH1840_tw.pdf");
      break;
    case "8":
      $("#name").text("耐環境氣侯 不鏽鋼 防爆型防護罩");
      $("#code").text("CPT-EXH1546");
      $("#type").text("防爆設備");
      $("#description").text(
        "-防爆適用場所 Class 1 DIV. 1 & 2, Zone 1 & 2, Group B & C\n-防撞擊鋼化(強化)光學視窗玻璃，適合高倍數鏡頭監控使用\n-內裝除霧電熱器並使用自動溫度開關控制\n-符合 CNS14165, CNS3376-0, 防塵防水 IP-68 標準\n- 適用安裝場所：煉油廠、加油站、瓦斯充氣站、汙水處理\n火力發電、礦場、晶圓、生物科技、化學工廠、海邊、海域等易\n燃、鹽害、腐蝕、易爆、油庫及彈藥庫等特殊環境場所"
      );
      $("#img_path").attr("src", "images/product/CPT-EXH1546.jpg");
      $("#doc_path").attr("href", "specs/CPT-EXH1546-tw.pdf");  
      break;
    case "9":
      $("#name").text("掌靜脈-PalmVEIN");
      $("#code").text("PalmVEIN");
      $("#type").text("生物特徵辨識");
      $("#description").text(
        "-經由手掌血管的熱輻射,取得血管的特徵\n-溫感攝影機處理器轉換 5MB 清晰圖像\n-無門口滯留設計, 網路不塞暴, 攻擊遠離,安全隱私 \n-採用多組格式同時傳送, 任何可上網設備即可連網 \n-快速,保密"
      );
      $("#img_path").attr("src", "images/product/palmvein.jpg");
      $("#doc_path").attr(
        "href",
        "specs/Catalog_Biological_PalmVEIN_CPT-NPV301_tw.pdf"
      );
      break;
    case "10":
      $("#name").text("室內全功能旋轉台(含自動旋轉)");
      $("#code").text("CPT-AP111T-100");
      $("#type").text("旋轉台");
      $("#description").text(
        "-符合 CE 國際安規\n-承載水平 Pan：7Kg / 15Lb，垂直 Tilt：4Kg / 9Lb\n-防火UL電纜 6pin空中引線接頭\n-由微動開關在水平及垂直極限位置保護\n-水平 Pan： 0° ～ 350°，垂直 Tilt：±35°"
      );
      $("#img_path").attr("src", "images/product/CPT-AP111T-100.jpg");
      $("#doc_path").attr("href", "specs/CPT-AP111T-tw-revised.pdf");
      break;
    case "11":
      $("#name").text("全天候全功能高負載旋轉台");
      $("#code").text("CPT-AP112T-100");
      $("#type").text("旋轉台");
      $("#description").text(
        "-防塵防水等級達IP-55.\n-旋轉角度水平：0°∼ 355°垂直最大角度可至 ±95°\n-硬化鋼齒輪驅動，延長迴轉台齒輪壽命\n-本體加強硬化及鉻酸處理，適用於抗酸檢腐蝕環境\n-垂直側臂配置鉛塊平衡"
      );
      $("#img_path").attr("src", "images/product/CPT-AP112T-100.jpg");
      $("#doc_path").attr("href", "specs/CPT-AP112T-tw.pdf");
      break;
    case "12":
      $("#name").text("抗強風型全功能高負載迴轉台");
      $("#code").text("CPT-AP115T");
      $("#type").text("旋轉台");
      $("#description").text(
        "防塵防水等級達IP-55-防風系數高,可承受空曠區域14級風之風力-硬化鋼齒輪驅動，延長迴轉台齒輪壽命-本體加強硬化及鉻酸處理，適用於抗酸檢腐蝕環境-垂直側臂配置鉛塊平"
      );
      $("#img_path").attr("src", "images/product/AP115T.jpg");
      $("#doc_path").attr("href", "specs/CPT-AP115T-tw.pdf");
      break;
    case "13":
      $("#name").text("全天候全功能旋轉台 & 高速預設型旋轉台");
      $("#code").text("CPT-PT35");
      $("#type").text("旋轉台");
      $("#description").text(
        "符合 IEC 529 防塵防水 IP-67 國際標準\n-符合 CE 國際安規\n-可選擇水平迴轉速度 6°/7.5°/10°/20°/30°/秒\n-硬化鋼齒輪驅動，延長迴轉台齒輪壽命\n-本體加強硬化及鉻酸處理，適用於抗酸檢腐蝕環境"
      );
      $("#img_path").attr("src", "images/product/PT25.jpg");
      $("#doc_path").attr("href", "specs/CPT-PT35-tw.pdf");
      break;
    case "14":
      $("#name").text("全天候全功能旋轉台 & 高速預設型旋轉台");
      $("#code").text("CPT-PT30");
      $("#type").text("旋轉台");
      $("#description").text(
        "-符合 IEC 529 防塵防水 IP-67 國際標準\n-符合 CE 國際安規\n-可選擇水平迴轉速度 6°/7.5°/10°/20°/30°/秒\n-硬化鋼齒輪驅動，延長迴轉台齒輪壽命\n-本體加強硬化及鉻酸處理，適用於抗酸檢腐蝕環境"
      );
      $("#img_path").attr("src", "images/product/PT30.jpg");
      $("#doc_path").attr("href", "specs/CPT-PT30-tw.pdf");
      break;
    case "15":
      $("#name").text("耐環境氣候 不鏽鋼 充氮式防護罩");
      $("#code").text("CPT-HS701-12N");
      $("#type").text("防爆設備");
      $("#description").text(
        "-適用於船艦，化學工業，食品工業，天然氣，煉油工業...等高侵蝕環境\n-符合IEC529 防壟防水 IP68國際標準\n-加裝環氧樹脂強化防水插頭\n-抗腐蝕成份高溫後加壓填充保護\n-不鏽鋼l SUS304 可調扎緊帶\n-符合IEC529 防壟防水 IP68國際標準\n-氣密條件規格符合20PSI"
      );
      $("#img_path").attr("src", "images/product/CPT-HS701-12N-100.jpg");
      $("#doc_path").attr("href", "specs/CPT-HS701-12N-tw.pdf");
      break;
    case "16": //12N
      $("#name").text("耐環境氣候 不鏽鋼 充氮式防護罩");
      $("#code").text("CPT-HS701-12N-IR");
      $("#type").text("防爆設備");
      $("#description").text(
        "-適用於船艦，化學工業，食品工業，天然氣，煉油工業...等高侵蝕環境\n-符合IEC529 防壟防水 IP68國際標準\n-投射距離 35公尺, DC12V 15W\n-加裝環氧樹脂強化防水插頭\n-抗腐蝕成份高溫後加壓填充保護\n-不鏽鋼l SUS304 可調扎緊帶\n-符合IEC529 防壟防水 IP68國際標準\n-氣密條件規格符合20PSI"
      );
      // $('#img_path').attr("src","images/product/CPT-HS701-12N-IR-100.jpg")
      // $('#img_path').attr("src","images/product/12N/DSC_0172-1.png")
      $("#doc_path").attr("href", "specs/CPT-HS701-12N-IR-tw.pdf");
      break;
    case "17": //12N-IR
      $("#name").text("耐環境氣候 不鏽鋼 充氮式防護罩");
      $("#code").text("CPT-HS701-12N-IR");
      $("#type").text("防爆設備");
      $("#description").text(
        "-適用於船艦，化學工業，食品工業，天然氣，煉油工業...等高侵蝕環境\n-符合IEC529防塵防水IP68國際標準\n-不鏽鋼AISI316L(JIS SUS316)可調扎緊帶\n-抗腐蝕成份高溫後加壓填充保護\n-強化安全玻離ø115mm，12mm厚度\n-惰性氣體：氮氣\n-可調整氣壓閥5～20PSI(出廠設定14.5PSI)"
      );
      // $('#img_path').attr("src","images/product/CPT-HS701-N316-100.jpg")
      // $('#img_path').attr("src","images/product/12N-IR/DSC_0305-1.png")
      // $('#img_arr_path').attr("src","$scope.productData.img_arr")
      $("#doc_path").attr("href", "specs/CPT-HS701-N316-tw.pdf");
      break;
    case "18":
      $("#name").text("超大型不鏽鋼　充氮式防護罩");
      $("#code").text("CPT-HS701-20N");
      $("#type").text("防爆設備");
      $("#description").text(
        "-適用於船艦，化學工業，食品工業，天然氣，煉油工業...等高侵蝕環境\n-符合IEC529防塵防水IP68國際標準\n-不鏽鋼AISI304L(JIS SUS304)可調扎緊帶\n-抗腐蝕成份高溫後加壓填充保護\n-強化安全玻離ψ200mm，12mm厚度\n-惰性氣體：氮氣\n-可調整氣壓閥5～20PSI(出廠設定14.5PSI)"
      );
      $("#img_path").attr("src", "images/product/18.png");
      $("#doc_path").attr("href", "specs/CPT-HS701-20N-tw.pdf");
      break;
    case "19":
      $("#name").text("流線型鋁製雙層遮陽防護罩");
      $("#code").text("CPT-HS701-10");
      $("#type").text("防爆設備");
      $("#description").text(
        "* 符合 CNS14165 (IEC60529) 防塵防水IP-68 國際標準。\n* 防護罩本體材質為不鏽鋼SUS304 材質 。\n* 雙層遮陽罩不鏽鋼 SUS304 避免日照直接曝曬及減少光害。\n* 採用光學玻璃視窗,折射率低&穿透性高,影像清晰不失真。\n* 可選用紅外熱像攝影機專用鍺玻璃Ge Glass。\n* 可選配自動溫控風扇、加熱除霧裝置。\n* 可選配雨刷及噴水清潔裝置,於遠端控制作動。\n* 可選配充氮型充氣洩氣閥裝置。\n* 可選配防水型網路 RJ-45 端口,方便安裝施作及維護。\n* 可選配不鏽鋼壁掛支架及調整座或不鏽鋼桿型夾具。\n* 適用安裝場所:煉油廠、加油站、瓦斯充氣站、汙水處理火\n力發電、礦場、晶圓、生物科技、化學工廠、海邊、海域及\n具鹽害、腐蝕等特殊環境場所。"
      );
      $("#img_path").attr("src", "images/product/19.png");
      $("#doc_path").attr("href", "specs/CPT-HS701-10.pdf");
      break;
    case "20":
      $("#name").text("固定支架 - 適用重型旋轉台");
      $("#code").text("CPT-BK702-7");
      $("#type").text("周邊設備");
      $("#description").text(
        "-用途說明:適用重型旋轉台\n-最大負載:500公斤\n-外觀尺寸:511mm(L) - 4.2公斤\nCPT-BK702-7T .511Lx230Wx230Hmm- 4.2公斤 (不鏽鋼304)\nCPT-BK702-7I .510Lx230Wx230Hmm- 4.2公斤 (不銹鐵)"
      );
      $("#img_path").attr("src", "images/product/20.png");
      $("#doc_path").attr("href", "specs/Catalog_Bracket_BK702-7T_7I-tw.pdf");
      break;
    case "21":
      $("#name").text("固定支架 - 適用重型旋轉台");
      $("#code").text("CPT-BK702-7S");
      $("#type").text("周邊設備");
      $("#description").text(
        "-用途說明:適用重型旋轉台\n-最大負載:200公斤\n-外觀尺寸:\nCPT-BK702-7ST .321Lx145.5Wx145.5Hmm-1.6公斤 (不鏽鋼304)\nCPT-BK702-7SI .320Lx145.5Wx145.5Hmm-1.25公斤(不銹鐵)"
      );
      $("#img_path").attr("src", "images/product/21.png");
      $("#doc_path").attr("href", "specs/Catalog_Bracket_BK702-7ST_7SI-tw.pdf");
      break;
    default:
    // code block
  }
});
