app.directive("chiperHeader", function() {
    return {
        template :' <header role="banner" ng-controller="headerController">'+
        '<nav class="navbar navbar-expand-lg " id="nav">'+
  '        <div class="container-fluid">'+
  '          <a class="navbar-brand " href="index.html"><img src="images/logo.png" alt="logo" style="width:120px"></a>'+
  '          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"'+
  '            aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">'+
  '            <span class="navbar-toggler-icon"></span>'+
  '          </button>'+
  '          <div class="collapse navbar-collapse" id="navbar">'+
  '            <ul class="navbar-nav pl-md-5">'+
  '              <li class="nav-item">'+
  '                <a class="nav-link" href="#!home" ng-click="collapseNav()">{{displayText.home}}</a>'+
  '              </li>'+
  '              <li class="nav-item">'+
  '                <a class="nav-link" href="#!about" ng-click="collapseNav()">{{displayText.about}}</a>'+
  '              </li>'+
  '              <li class="nav-item dropdown">'+
  '                <a class="nav-link" href="#!project" ng-click="collapseNav()">{{displayText.project}}</a>'+
  '              </li>'+
  '              <li class="nav-item">'+
  '                <a class="nav-link" href="#!product" ng-click="collapseNav()">{{displayText.product}}</a>'+
  '              </li>'+
  '              <li class="nav-item">'+
  '                <a class="nav-link" href="#!carplate-road" ng-click="collapseNav()">{{displayText.carplate}}</a>'+
  '              </li>'+
  '              <li class="nav-item">'+
  '                <a class="nav-link" href="#!contact" ng-click="collapseNav()">{{displayText.contact}}</a>'+
  '              </li>'+
  '              <li class="nav-item dropdown">'+
  '                <a class="nav-link dropdown-toggle" id="dropdown04" data-toggle="dropdown" aria-haspopup="true"'+
  '                  aria-expanded="false">{{displayText.lang}}</a>'+
  '                <div class="dropdown-menu" aria-labelledby="dropdown04">'+
  '                  <a class="dropdown-item" href="javascript:void(0)" ng-click="changeLang(1)" >中文</a>'+
  '                  <a class="dropdown-item" href="javascript:void(0)" ng-click="changeLang(2)">English</a>'+
  '                </div>'+
  '              </li>'+
  '            </ul>'+
  '          </div>'+
  '        </div>'+
  '      </nav>'+
  '    </header>'
    };
});