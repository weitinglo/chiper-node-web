app.directive("chiperFooter", function() {
    return {
        template : 
      
        ' <footer class="site-footer" role="contentinfo" ng-controller="footerController">'+
        '<div class="go-top" style="display: block;" ng-click="scrollTop()">'+
          '<i class="icon-arrow-up" aria-hidden="true"></i>'+
        '</div>'+
        '<div class="container">'+
          '<div class="row">'+
            '<div class="col-md-6 aboutPart">'+
              '<h3 class="mb-4">{{displayText.about}}</h3>'+
              '<p class="text-white-customer">{{displayText.aboutContent}}</p>'+
        
            '</div>'+
           ' <div class="col-md-6 pl-md-5">'+
              '<div>'+
                '<h3 class="mb-4">{{displayText.contact}}</h3>'+
                '<ul class="list-unstyled footer-link">'+
                 ' <li class="d-block">'+
                    '<span class="d-block text-white-customer">{{displayText.addressTitle}}:</span>'+
                   ' <span class="text-white-customer">{{displayText.address}}</span>'+
                  '</li>'+
                  '<li class="d-block">'+
                   ' <span class="d-block text-white-customer">{{displayText.phoneTitle}}:</span><span class="text-white-customer">{{displayText.phone}}</span>'+
                   '</li>'+
                  '<li class="d-block">'+                  
                    '<span class="d-block text-white-customer">Email:</span><span class="text-white-customer">{{displayText.email}}</span>'+
                  '</li>'+
                '</ul>'+
              '</div>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</footer>'
    };
});