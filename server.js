const express = require("express");
const app = express();
const port = 80;
const httpsPort = 443;
const sqlite3 = require("sqlite3").verbose();
const { GoogleAuth } = require("google-auth-library");
var bodyParser = require("body-parser");
var nodemailer = require("nodemailer");
var http = require("https");
var https = require("https");
var fs = require("fs");
require("dotenv").config();

app.enable("trust proxy");

app.use(function (req, res, next) {
  let reqDomain = req.headers.host.split(":")[0];
  let reqPort = req.headers.host.split(":")[1];
  if (!req.secure) {
    if (reqDomain.includes("chiper"))
      return res.redirect("https://www.chiper.com.tw" + req.url);
    else
      return res.redirect("https://" + reqDomain + ":" + httpsPort + req.url);
  }
  next();
});

let db = new sqlite3.Database("chiperDB.db", (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Connected to the in-memory SQlite database.");
});

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.static("view"));
app.use(express.static("assets/"));

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/view/" + "main.html");
});

app.get("/project", function (req, res) {
  let sqlQuery = "SELECT * FROM PROJECT ORDER BY ORDERNUM";
  db.all(sqlQuery, [], (err, rows) => {
    if (err) {
      let jsonMsg = {
        msg: err.message,
        code: 500,
      };
      res.send(jsonMsg);
    } else {
      res.send(rows);
    }
  });
});

app.get("/project_detail", function (req, res) {
  let projectId = req.query.projectId;
  let sqlQuery = "SELECT * FROM PROJECT WHERE ID =" + projectId;
  db.all(sqlQuery, [], (err, rows) => {
    if (err) {
      let jsonMsg = {
        msg: err.message,
        code: 500,
      };
      res.send(jsonMsg);
    } else {
      res.send(rows);
    }
  });
});

app.get("/product", function (req, res) {
  let lang = req.query.lang;
  let sqlQuery =
    "SELECT * FROM PRODUCT LEFT JOIN PRODUCT_TRANSLATION ON PRODUCT.id = PRODUCT_TRANSLATION.product_id WHERE PRODUCT_TRANSLATION.lan_id = ? AND PRODUCT.SHOW = 1";
  db.all(sqlQuery, [lang], (err, rows) => {
    if (err) {
      let jsonMsg = {
        msg: err.message,
        code: 500,
      };
      res.send(jsonMsg);
    } else {
      // console.log(rows);
      res.send(rows);
    }
  });
});
app.get("/product_detail", function (req, res) {
  let productId = req.query.productId;
  let lang = req.query.lang;
  // let sqlQuery = "SELECT * FROM PRODUCT WHERE ID =" + productId;
  let sqlQuery =
    "SELECT * FROM PRODUCT LEFT JOIN PRODUCT_TRANSLATION ON PRODUCT.id = PRODUCT_TRANSLATION.product_id WHERE PRODUCT_TRANSLATION.lan_id = ? AND PRODUCT.SHOW = 1 AND PRODUCT.id = ?";

  db.all(sqlQuery, [lang, productId], (err, rows) => {
    if (err) {
      let jsonMsg = {
        msg: err.message,
        code: 500,
      };
      res.send(jsonMsg);
    } else {
      if (rows.length >= 0) {
        if (!rows || !rows[0] || !rows[0].lan_id) res.send([]);
        else {
          if (lang != rows[0].lan_id) {
            let newProductID = rows[0].lan_id;
            sqlQuery = "SELECT * FROM PRODUCT WHERE ID =" + newProductID;
            db.all(sqlQuery, [], (err, rows) => {
              if (err) {
                let jsonMsg = {
                  msg: err.message,
                  code: 500,
                };
                res.send(jsonMsg);
              } else {
                res.send(rows);
              }
            });
          } else {
            res.send(rows);
          }
        }
      }
    }
  });
});

app.get("/product_type", function (req, res) {
  let lang = req.query.lang;
  let sqlQuery =
    "SELECT DISTINCT(TYPE) FROM PRODUCT_TRANSLATION WHERE LAN_ID=?";
  db.all(sqlQuery, [lang], (err, rows) => {
    if (err) {
      let jsonMsg = {
        msg: err.message,
        code: 500,
      };
      res.send(jsonMsg);
    } else {
      res.send(rows);
    }
  });
});

app.get("/product_detail", function (req, res) {
  let productId = req.query.productId;
  let lang = req.query.lang;
  lang = lang == "1" ? "ch" : "en";
  let sqlQuery = "SELECT * FROM PRODUCT WHERE ID =" + productId;
  db.all(sqlQuery, [], (err, rows) => {
    if (err) {
      let jsonMsg = {
        msg: err.message,
        code: 500,
      };
      res.send(jsonMsg);
    } else {
      if (rows.length >= 0) {
        if (lang != rows[0].lan) {
          let newProductID = rows[0].lan_id;
          sqlQuery = "SELECT * FROM PRODUCT WHERE ID =" + newProductID;
          db.all(sqlQuery, [], (err, rows) => {
            if (err) {
              let jsonMsg = {
                msg: err.message,
                code: 500,
              };
              res.send(jsonMsg);
            } else {
              res.send(rows);
            }
          });
        } else {
          res.send(rows);
        }
      }
    }
  });
});

app.post("/get_contact", function (req, res) {
  let name =
    typeof req.body.name !== "undefined" && req.body.name != ""
      ? req.body.name
      : null;
  let pwd =
    typeof req.body.pwd !== "undefined" && req.body.pwd != ""
      ? req.body.pwd
      : null;
  let jsonMsg = {
    msg: "資料有問題",
    code: 412,
  };

  if (name == "" || name == null || pwd == "" || pwd == null) {
    res.send(jsonMsg);
  } else {
    if (name == "chiper" && pwd == "chiper9101") {
      let sqlQuery = "SELECT * FROM CONTACT ORDER BY ID DESC";
      db.all(sqlQuery, [], (err, rows) => {
        if (err) {
          let jsonMsg = {
            msg: err.message,
            code: 500,
          };
          res.send(jsonMsg);
        } else {
          res.send(rows);
        }
      });
    } else {
      res.send(jsonMsg);
    }
  }
});

async function sendEmail(toEmail, emailTitle, emailMsg) {
  console.log(process.env.GMAIL_PASS);
  return new Promise((resolve, reject) => {
    var transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.GMAIL_USER,
        pass: process.env.GMAIL_PASS,
      },
    });

    var mailOptions = {
      from: process.env.GMAIL_USER,
      to: toEmail,
      cc: "weitinglo@lot.systems",
      subject: emailTitle,
      html: emailMsg,
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.error("Email sending error:", error);
        reject(error);
      } else {
        console.log("Email sent:", info.response);
        resolve(info);
      }
    });
  });
}
app.post("/contact_message", async function (req, res) {
  console.log("Received contact message request:", req.body);
  let name = req.body.name || null;
  let phone = req.body.phone || null;
  let email = req.body.email || null;
  let sub = req.body.sub || null;
  let message = req.body.message || null;

  let date = new Date();
  const offset = date.getTimezoneOffset();
  date = new Date(date.getTime() - offset * 60 * 1000);
  let datetime = date.toISOString().replace("T", " ").split(".")[0];

  let jsonMsg = {
    msg: "資料有問題",
    code: 412,
  };

  if (!name || !email || !sub) {
    res.send(jsonMsg);
  } else {
    let fullMessage = `留言名字：${name}| EMAIL：${email}| 電話：${phone}| 主題：${sub}| 留言：${message}`;

    try {
      // await sendEmail("support@chiper.com.tw", "網站留言", fullMessage);

      db.run(
        "INSERT INTO CONTACT(NAME,EMAIL,PHONE,SUB,MESSAGE,DATETIME) VALUES ($name, $email, $phone, $sub, $message, $datetime)",
        {
          $name: name,
          $phone: phone,
          $email: email,
          $sub: sub,
          $message: message,
          $datetime: datetime,
        },
        function (err) {
          if (err) {
            jsonMsg.msg = err.message;
            jsonMsg.code = 500;
            res.send(jsonMsg);
          } else {
            jsonMsg.msg = "傳送成功";
            jsonMsg.code = 200;
            res.send(jsonMsg);
          }
        }
      );
    } catch (error) {
      jsonMsg.msg = "郵件發送失敗";
      jsonMsg.code = 500;
      res.send(jsonMsg);
    }
  }
});

var credentials = {
  key: fs.readFileSync("./cert-2024/www.chiper.com.tw.2024.key"),
  cert: fs.readFileSync("./cert-2024/www.chiper.com.tw.2024.crt"),
  ca: [
    fs.readFileSync("./cert-2024/SectigoChainOVbundle.crt"),
    fs.readFileSync("./cert-2024/Sectigo Chain OV.crt"),
    fs.readFileSync("./cert-2024/Sectigo Root.crt"),
  ],
};

app.listen(
  port,
  (req, res) =>
    function () {
      console.log(req);
      console.log(`Example app listening on port ${port}!`);
    }
);
var httpsServer = https.createServer(credentials, app);
httpsServer.listen(httpsPort);
